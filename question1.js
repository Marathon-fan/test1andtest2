var Promise = require('promise');

module.exports = class Question1{

    constructor() {
        this.endpoint1 = 'https://join.reckon.com/test1/rangeInfo';
        this.endpoint2 = 'https://join.reckon.com/test1/divisorInfo';
        this.divisorMap = new Map();
        this.lower = 0;
        this.upper = 0;
        this.data = [];
        this.request = require('promise-request-retry'); 
    }

    callEndpoint(){
       var theObj = this;

       // call endpoint 1
       this.v1 = this.request({
            "method":"GET", 
            "uri": this.endpoint1,
            "json": true,
            "retry" : 5          
          }).then(function(response){
              //console.log('response', response);
              theObj.lower = parseInt(response.lower);
              theObj.upper = parseInt(response.upper);
              return response;
          }, 
          console.log);

        // call endpoint 2
        this.v2 = this.request({
            "method":"GET", 
            "uri": this.endpoint2,
            "json": true,
            "retry" : 5
          }).then(function(response){
              //console.log('response', response);
              response.outputDetails.forEach(element => {
                  theObj.divisorMap.set(element.divisor, element.output);                   
              });
              return response;
          }, 
          console.log);

          // begin the calculation
          Promise.all([this.v1, this.v2])
          .then(res => {
              var lower = Math.max(1, this.lower);
              var upper = this.upper;
              var data = [];
              for (let i = lower; i <= upper; i++) {
                var output = '';
                for (let [key, value] of this.divisorMap) {
                    if ( i % key === 0) {
                        output += value;
                    } 
                }
                var tObj = new Object()
                tObj[i] = output;  
                theObj.data.push(tObj);
                //console.log(output);
              }
              console.log(theObj.data);
          })
          .catch(err => console.log('error', err))   
    }   
   
};